
package com.marcin.springboottmpl.validators;

import com.marcin.springboottmpl.northwind.Orders;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;


@Component
public class OrdersValidator implements Validator {

    @Override
    public boolean supports(Class<?> paramClass) {
        return Orders.class.equals(paramClass);
    }

    @Override
    public void validate(Object obj, Errors errors) {
        Orders order = (Orders) obj;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "customers", "valid.customers");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "Shippers", "valid.shippers");
    }
    
}
