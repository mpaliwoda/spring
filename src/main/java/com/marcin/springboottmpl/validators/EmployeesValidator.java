
package com.marcin.springboottmpl.validators;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.marcin.springboottmpl.northwind.Employees;


@Component
public class EmployeesValidator implements Validator {

    
    @Override
    public boolean supports(Class<?> paramClass) {
        return Employees.class.equals(paramClass);
    }

    
    @Override
    public void validate(Object obj, Errors errors) {
        Employees employee = (Employees) obj;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "lastName", "valid.name");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "firstName", "valid.name");

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "birthDate", "valid.birthDate");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "city", "valid.city");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "postalCode", "valid.postalCode");
    }
}
