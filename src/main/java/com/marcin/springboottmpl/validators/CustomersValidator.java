
package com.marcin.springboottmpl.validators;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import com.marcin.springboottmpl.northwind.Customers;
import org.springframework.stereotype.Component;
import org.springframework.validation.ValidationUtils;

@Component
public class CustomersValidator implements Validator {

    @Override
    public boolean supports(Class<?> paramClass) {
        return Customers.class.equals(paramClass);
    }

    @Override
    public void validate(Object obj, Errors errors) {
        Customers customer = (Customers) obj;
        
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "companyName", "valid.companyName");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "phone", "valid.phone");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "contactName", "valid.contactName");
    }
    
}
