/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.marcin.springboottmpl.validators;

import org.springframework.stereotype.Component;
import com.marcin.springboottmpl.northwind.Categories;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class CategoriesValidator implements Validator {

    @Override
    public boolean supports(Class<?> paramClass) {
        return Categories.class.equals(paramClass);
    }

    @Override
    public void validate(Object obj, Errors errors) {
        Categories category = (Categories) obj;
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "categoryName", "valid.name");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "description", "valid.description");
    }
    
}
