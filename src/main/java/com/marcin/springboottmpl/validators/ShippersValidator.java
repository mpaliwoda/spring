
package com.marcin.springboottmpl.validators;

import com.marcin.springboottmpl.northwind.Shippers;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;


@Component
public class ShippersValidator implements Validator {

    @Override
    public boolean supports(Class<?> type) {
        return Shippers.class.equals(type);
    }

    @Override
    public void validate(Object o, Errors errors) {
        Shippers shipper = (Shippers) o;
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "companyName", "valid.companyName");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "phone", "valid.phone");
    }
    
}
