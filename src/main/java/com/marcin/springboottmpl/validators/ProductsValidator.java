
package com.marcin.springboottmpl.validators;

import com.marcin.springboottmpl.northwind.Products;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;


@Component
public class ProductsValidator implements Validator {

    @Override
    public boolean supports(Class<?> paramClass) {
        return Products.class.equals(paramClass);
    }

    @Override
    public void validate(Object obj, Errors errors) {
        Products product = (Products) obj;
        
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "productName", "valid.productName");
    }
}
