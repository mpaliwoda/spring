package com.marcin.springboottmpl.controllers;

import com.marcin.springboottmpl.northwind.Customers;
import com.marcin.springboottmpl.northwind.Employees;
import com.marcin.springboottmpl.northwind.OrderDetails;
import com.marcin.springboottmpl.northwind.OrderDetailsId;
import com.marcin.springboottmpl.northwind.Shippers;
import com.marcin.springboottmpl.repositories.OrdersRepository;
import com.marcin.springboottmpl.repositories.ShippersRepository;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.marcin.springboottmpl.northwind.Orders;
import com.marcin.springboottmpl.northwind.Products;
import com.marcin.springboottmpl.repositories.CustomersRepository;
import com.marcin.springboottmpl.repositories.EmployeesRepository;
import com.marcin.springboottmpl.repositories.ProductsRepository;
import com.marcin.springboottmpl.viewmodels.OrdersViewModel;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
public class OrdersController {

    private static final Logger LOGGER = LoggerFactory.getLogger(OrdersController.class);

    @Autowired
    protected OrdersRepository ordersRepository;

    @Autowired
    protected CustomersRepository customersRepository;

    @Autowired
    protected ShippersRepository shippersRepository;

    @Autowired
    protected EmployeesRepository employeesRepository;

    @Autowired
    protected ProductsRepository productsRepository;

    @RequestMapping(value = "/Orders/Index", method = RequestMethod.GET)
    public ModelAndView ordersHandler() {
        LOGGER.debug("ordersHandler()");

        ModelAndView view = new ModelAndView();
        view.setViewName("/Orders/Index");

        Iterable<Orders> orders = ordersRepository.findAll();
        view.addObject("Orders", orders);

        return view;
    }

    private String OrderSessionHelper(Integer orderId) {
        return "/Orders/Edit/" + orderId.toString();
    }

    @RequestMapping(value = "/Orders/Edit/{orderId}", method = RequestMethod.GET)
    public ModelAndView orderHandler(@PathVariable Integer orderId, HttpSession session) throws ParseException {
        ModelAndView view = new ModelAndView();
        view.setViewName("/Orders/Edit");
        Iterable<Customers> customers = customersRepository.findAll();
        view.addObject("Customers", customers);
        Iterable<Shippers> shippers = shippersRepository.findAll();
        view.addObject("Shippers", shippers);
        Iterable<Employees> employees = employeesRepository.findAll();
        view.addObject("Employees", employees);
        Iterable<Products> products = productsRepository.findAll();
        view.addObject("Products", products);
        List<OrderDetails> orderDetails = null;

        Double price = 0.;

        Orders order = ordersRepository.findByOrderId(orderId);
        if (order != null) {
            HashMap saver = (HashMap) session.getAttribute(OrderSessionHelper(orderId));
            if (saver != null) {
                Customers customer = customersRepository.findByCustomerId(saver.get("customer").toString());
                order.setCustomers(customer);
                order.setShippers(shippersRepository.findByShipperId(Integer.parseInt(saver.get("shipper").toString())));
                order.setEmployees(employeesRepository.findByEmployeeId(Integer.parseInt(saver.get("employee").toString())));
                order.setShipName(customer.getCompanyName());
                order.setShipAddress(customer.getAddress());
                order.setShipPostalCode(customer.getPostalCode());
                order.setShipRegion(customer.getRegion());
                order.setShipCity(customer.getCity());
                order.setShipCountry(customer.getCountry());

                DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                Date orderDate = dateFormat.parse(saver.get("orderDate").toString());
                Date shippedDate = dateFormat.parse(saver.get("shippedDate").toString());
                order.setOrderDate(orderDate);
                order.setShippedDate(shippedDate);

                orderDetails = (List<OrderDetails>) session.getAttribute("orderDetails");
                //Set<OrderDetails> temp = order.getOrderDetailses();
                order.getOrderDetailses().clear();
                for (OrderDetails z : orderDetails) {
                    order.getOrderDetailses().add(z);
                    price += z.getUnitPrice().doubleValue() * z.getQuantity();
                }
                session.setAttribute(OrderSessionHelper(orderId), null);

                ordersRepository.save(order);
            } else {
                orderDetails = new ArrayList<OrderDetails>();
                orderDetails.addAll(order.getOrderDetailses());
                session.setAttribute("orderDetails", orderDetails);
                for (OrderDetails z : orderDetails) {
                    price += z.getUnitPrice().doubleValue() * z.getQuantity();
                }
            }
            view.addObject("price", price);
            view.addObject("order", order);
            view.addObject("orderDetails", orderDetails);
        }
        return view;
    }

    @RequestMapping(value = "/Orders/Edit/{orderId}", method = RequestMethod.POST)
    public String orderHandler(@PathVariable Integer orderId, HttpSession session, HttpServletRequest request) {
        Map saver = new HashMap();
        saver.put("customer", request.getParameter("customer"));
        saver.put("shipper", request.getParameter("shipper"));
        saver.put("employee", request.getParameter("employee"));
        saver.put("orderDate", request.getParameter("orderDate"));
        saver.put("shippedDate", request.getParameter("shippedDate"));

        session.setAttribute(OrderSessionHelper(orderId), saver);

        return "redirect:" + OrderSessionHelper(orderId);
    }

    @RequestMapping(value = "/Orders/Edit", method = RequestMethod.POST)
    public ModelAndView orderHandlerHelper(@ModelAttribute OrdersViewModel order, BindingResult result, HttpSession session) throws ParseException {
        ModelAndView view = new ModelAndView();
        view.setViewName("/Orders/Edit");

        Iterable<Customers> customers = customersRepository.findAll();
        view.addObject("Customers", customers);
        Iterable<Shippers> shippers = shippersRepository.findAll();
        view.addObject("Shippers", shippers);
        Iterable<Employees> employees = employeesRepository.findAll();
        view.addObject("Employees", employees);
        Iterable<Products> products = productsRepository.findAll();
        view.addObject("Products", products);

        List<OrderDetails> orderDetails = (List<OrderDetails>) session.getAttribute("orderDetails");

        Orders ord = ordersRepository.findByOrderId(order.getOrderId());

        Customers customer = customersRepository.findByCustomerId(order.getCustomer());
        ord.setCustomers(customer);
        ord.setShippers(shippersRepository.findByShipperId(Integer.parseInt(order.getShipper())));
        ord.setEmployees(employeesRepository.findByEmployeeId(Integer.parseInt(order.getEmployee())));
        ord.setShipName(customer.getCompanyName());
        ord.setShipAddress(customer.getAddress());
        ord.setShipPostalCode(customer.getPostalCode());
        ord.setShipRegion(customer.getRegion());
        ord.setShipCity(customer.getCity());
        ord.setShipCountry(customer.getCountry());

        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        Date orderDate = dateFormat.parse(order.getOrderDate());
        Date shippedDate = dateFormat.parse(order.getShippedDate());
        ord.setOrderDate(orderDate);
        ord.setShippedDate(shippedDate);
        Double price = 0.;
        for (OrderDetails z : orderDetails) {
            price += z.getUnitPrice().doubleValue() * z.getQuantity();
        }
        view.addObject("price", price);

        view.addObject("order", ord);
        view.addObject("orderDetails", orderDetails);

        return view;
    }

    @RequestMapping(value = "/Orders/Edit/{orderId}/{productId}", method = RequestMethod.POST)
    public ModelAndView orderRemoveProductHandler(@PathVariable Integer orderId, @PathVariable Integer productId, @ModelAttribute OrdersViewModel order, BindingResult result, HttpSession session) throws ParseException {
        List<OrderDetails> orderDetails = (List<OrderDetails>) session.getAttribute("orderDetails");

        OrderDetails found = null;
        for (OrderDetails d : orderDetails) {
            OrderDetailsId id = d.getId();
            if (id.getOrderId() == orderId && id.getProductId() == productId) {
                found = d;
                break;
            }
        }
        if (found != null) {
            orderDetails.remove(found);
            session.setAttribute("orderDetails", orderDetails);
        }

        return orderHandlerHelper(order, result, session);
    }

    @RequestMapping(value = "/Orders/Edit/AddProduct/{orderId}", method = RequestMethod.POST)
    public ModelAndView orderAddProductHandler(@PathVariable Integer orderId, @ModelAttribute OrdersViewModel order, BindingResult result, HttpSession session, HttpServletRequest request) throws ParseException {
        Integer productId = Integer.parseInt(request.getParameter("product"));

        List<OrderDetails> orderDetails = (List<OrderDetails>) session.getAttribute("orderDetails");
        Products pr = productsRepository.findByProductId(Integer.parseInt(request.getParameter("product")));

        OrderDetailsId id = new OrderDetailsId();
        id.setOrderId(orderId);
        id.setProductId(productId);

        OrderDetails od = new OrderDetails();
        od.setId(id);
        od.setOrders(ordersRepository.findByOrderId(orderId));
        od.setProducts(pr);
        od.setUnitPrice(pr.getUnitPrice());
        od.setQuantity(Short.parseShort(request.getParameter("quantity")));

        orderDetails.add(od);
        session.setAttribute("orderDetails", orderDetails);

        return orderHandlerHelper(order, result, session);
    }

    @RequestMapping(value = "/Orders/Add", method = RequestMethod.GET)
    public ModelAndView orderAddHandler(HttpSession session) throws ParseException {
        ModelAndView view = new ModelAndView();
        view.setViewName("/Orders/Add");
        Iterable<Customers> customers = customersRepository.findAll();
        view.addObject("Customers", customers);
        Iterable<Shippers> shippers = shippersRepository.findAll();
        view.addObject("Shippers", shippers);
        Iterable<Employees> employees = employeesRepository.findAll();
        view.addObject("Employees", employees);

        Orders empty = new Orders();
        view.addObject("order", empty);

        return view;
    }

    @RequestMapping(value = "/Orders/Add", method = RequestMethod.POST)
    public String orderAddHandlerPOST(HttpSession session, HttpServletRequest request) throws ParseException {
        Orders order = new Orders();
        Customers customer = customersRepository.findByCustomerId(request.getParameter("customer"));
        order.setCustomers(customer);
        order.setShippers(shippersRepository.findByShipperId(Integer.parseInt(request.getParameter("shipper"))));
        order.setEmployees(employeesRepository.findByEmployeeId(Integer.parseInt(request.getParameter("employee"))));
        order.setShipName(customer.getCompanyName());
        order.setShipAddress(customer.getAddress());
        order.setShipPostalCode(customer.getPostalCode());
        order.setShipRegion(customer.getRegion());
        order.setShipCity(customer.getCity());
        order.setShipCountry(customer.getCountry());
        
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        Date orderDate = dateFormat.parse(request.getParameter("orderDate"));
        order.setOrderDate( orderDate );

        Orders savedOrder = ordersRepository.saveAndFlush(order);

        return "redirect:/Orders/Edit/" + savedOrder.getOrderId();
    }

    @RequestMapping(value = "/Orders/Add/CustomerSet", method = RequestMethod.POST)
    public ModelAndView orderAddHandlerHelper(HttpSession session, HttpServletRequest request) {
        ModelAndView view = new ModelAndView();
        view.setViewName("/Orders/Add");

        Iterable<Customers> customers = customersRepository.findAll();
        view.addObject("Customers", customers);
        Iterable<Shippers> shippers = shippersRepository.findAll();
        view.addObject("Shippers", shippers);
        Iterable<Employees> employees = employeesRepository.findAll();
        view.addObject("Employees", employees);
        Iterable<Products> products = productsRepository.findAll();
        view.addObject("Products", products);

        Orders ord = new Orders();

        Customers customer = customersRepository.findByCustomerId(request.getParameter("customer"));
        ord.setCustomers(customer);
        ord.setShipName(customer.getCompanyName());
        ord.setShipAddress(customer.getAddress());
        ord.setShipPostalCode(customer.getPostalCode());
        ord.setShipRegion(customer.getRegion());
        ord.setShipCity(customer.getCity());
        ord.setShipCountry(customer.getCountry());

        view.addObject("order", ord);

        return view;
    }

    @RequestMapping(value = "/Orders/Remove/{orderId}", method = RequestMethod.GET)
    public String orderRemoveHandler(@PathVariable Integer orderId) {
        Orders ord = ordersRepository.findByOrderId(orderId);
        ordersRepository.delete(ord);

        return "redirect:/Orders/Index";
    }
}
