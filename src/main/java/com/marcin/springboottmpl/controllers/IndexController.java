package com.marcin.springboottmpl.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Controller
public class IndexController {
    private static final Logger LOGGER = LoggerFactory.getLogger(IndexController.class);

    @RequestMapping("/")
    public String homeHandler() { return "Index"; }

    @RequestMapping(value = "/Index", method = RequestMethod.GET)
    public String indexHandler() { return "Index"; }    
}
