package com.marcin.springboottmpl.controllers;

import com.marcin.springboottmpl.repositories.CategoriesRepository;
import com.marcin.springboottmpl.northwind.Categories;
import com.marcin.springboottmpl.repositories.ProductsRepository;
import com.marcin.springboottmpl.validators.CategoriesValidator;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
public class CategoriesController {

    private static final Logger LOGGER = LoggerFactory.getLogger(IndexController.class);

    @Autowired
    protected CategoriesRepository categoriesRepository;
    @Autowired
    protected ProductsRepository productsRepository;
    @Autowired
    protected CategoriesValidator validator;
    @InitBinder
    protected void initBinder(WebDataBinder binder){
        binder.setValidator(validator);
    }

    @RequestMapping(value = "/Categories/Index", method = RequestMethod.GET)
    public ModelAndView categoriesHandler() {
        LOGGER.debug("categoriesHandler()");

        ModelAndView view = new ModelAndView();
            view.setViewName("/Categories/Index");

        Iterable<Categories> categories = categoriesRepository.findAll();
            view.addObject("Categories", categories);

        return view;
    }

    private String CategorySessionHelper(Integer categoryId) {
        return "/Categories/Edit/" + categoryId.toString();
    }

    @RequestMapping(value = "/Categories/Edit/{categoryId}", method = RequestMethod.GET)
    public ModelAndView categoryHandler(@PathVariable int categoryId, HttpSession session) {
        ModelAndView view = new ModelAndView();
        view.setViewName("/Categories/Edit");

        Categories category = (Categories) session.getAttribute(CategorySessionHelper(categoryId));
        if (category == null) {
            category = categoriesRepository.findByCategoryId(categoryId);
        } else {
            session.setAttribute(CategorySessionHelper(categoryId), null);
            categoriesRepository.save(category);
        }
        
         if( session.getAttribute("errors") != null ) {
            view.addObject( "errors", session.getAttribute("errors") );
            session.setAttribute("errors", null);
        }
        
        view.addObject("cat", category);
        return view;
    }

    @RequestMapping(value = "/Categories/Edit/{categoryId}", method = RequestMethod.POST)
    public String categoryHandlerPOST(@Valid @ModelAttribute Categories category, BindingResult result, @PathVariable int categoryId, HttpSession session) {
        validator.validate(category, result);
        if(result.hasFieldErrors()) {
            session.setAttribute("errors", result.getAllErrors());
        } else {
            session.setAttribute(CategorySessionHelper(categoryId), category);
        }
        
        return "redirect:" + CategorySessionHelper(categoryId);
    }

    @RequestMapping(value = "/Categories/Add", method = RequestMethod.GET)
    public ModelAndView categoryAddHandler( HttpSession session ) {
        ModelAndView view = new ModelAndView();
            view.setViewName("/Categories/Add");
            
        if( session.getAttribute("errors") != null ) {
            view.addObject( "errors", session.getAttribute("errors") );
            session.setAttribute("errors", null);
        }
            
        return view;
    }

    @RequestMapping(value = "/Categories/Add", method = RequestMethod.POST)
    public String categoryAddHandlerPOST(@Valid @ModelAttribute Categories category, BindingResult result, HttpSession session) {
        validator.validate(category, result);
        if(result.hasFieldErrors()) {
            session.setAttribute("errors", result.getAllErrors());
            return "redirect:/Categories/Add";
        }
        category = categoriesRepository.saveAndFlush(category);

        return "redirect:/Categories/Index/";
    }

    @RequestMapping(value = "/Categories/Remove/{categoryId}", method = RequestMethod.GET)
    public String categoryRemoveHandler(@PathVariable Integer categoryId) {
        Categories cat = categoriesRepository.findByCategoryId(categoryId);
        categoriesRepository.delete(cat);

        return "redirect:/Categories/Index";
    }
}
