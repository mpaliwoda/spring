package com.marcin.springboottmpl.controllers;

import com.marcin.springboottmpl.repositories.EmployeesRepository;
import com.marcin.springboottmpl.northwind.Employees;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import com.marcin.springboottmpl.validators.EmployeesValidator;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
public class EmployeesController {

    private static final Logger LOGGER = LoggerFactory.getLogger(IndexController.class);

    @Autowired
    protected EmployeesRepository employeesRepository;

    @Autowired
    protected EmployeesValidator validator;

    @InitBinder("employee")
    protected void initBinder(WebDataBinder binder) {
        binder.setValidator(validator);
    }

    @RequestMapping(value = "/Employees/Index", method = RequestMethod.GET)
    public ModelAndView employeesHandler() {
        LOGGER.debug("employeesHandler()");

        ModelAndView view = new ModelAndView();
        view.setViewName("/Employees/Index");

        Iterable<Employees> employees = employeesRepository.findAll();
        view.addObject("Employees", employees);

        return view;
    }

    private String EmployeeSessionHelper(Integer employeeId) {
        return "/Employees/Edit/" + employeeId.toString();
    }

    @RequestMapping(value = "/Employees/Edit/{employeeId}", method = RequestMethod.GET)
    public ModelAndView employeeHandler(@PathVariable Integer employeeId, HttpSession session) {
        ModelAndView view = new ModelAndView();
            view.setViewName("/Employees/Edit");
        
        Employees employee = (Employees) session.getAttribute(EmployeeSessionHelper(employeeId));
        if (employee == null) {
            employee = employeesRepository.findByEmployeeId(employeeId);
        } else {
            employeesRepository.save(employee);
            session.setAttribute(EmployeeSessionHelper(employeeId), null);
        }
        
        if( session.getAttribute("errors") != null ) {
            view.addObject( "errors", session.getAttribute("errors") );
            session.setAttribute("errors", null);
        }

        view.addObject("em", employee);

        return view;
    }

    @RequestMapping(value = "/Employees/Edit/{employeeId}", method = RequestMethod.POST)
    public String employeeHandlerPOST(@Valid @ModelAttribute Employees employee, BindingResult result, @PathVariable Integer employeeId, HttpSession session) {
        validator.validate(employee, result);
        if (result.hasErrors()) {
            session.setAttribute("errors", result.getAllErrors());
        } else {
            session.setAttribute(EmployeeSessionHelper(employeeId), employee);
        }

        return "redirect:" + EmployeeSessionHelper(employeeId);
    }

    /*
    @RequestMapping(value="/Employees/Edit/{employeeId}", method = RequestMethod.GET)
    public ModelAndView employeeHandler( @PathVariable Integer employeeId, Model uiModel, HttpSession session ) {
        ModelAndView view = new ModelAndView();
            view.setViewName("/Employees/Edit");

        Employees employee = employeesRepository.findByEmployeeId( employeeId );

        Model uiModelSession  = ( Model ) session.getAttribute( EmployeeSessionHelper( employeeId ) );
        if( uiModelSession != null ) {
            int i = 0;
        } else {
            uiModel.addAttribute( employee );
        }

        view.addObject("em", employee);

        return view;
    }
    @RequestMapping(value="/Employees/Edit/{employeeId}", method = RequestMethod.POST)
    public String employeeHandlerPOST( @Valid Employees employee, HttpSession session ) {

        session.setAttribute( EmployeeSessionHelper( employee.getEmployeeId() ), employee );

        return "redirect:" + EmployeeSessionHelper( employee.getEmployeeId() );
    }
     */
    @RequestMapping(value = "/Employees/Add", method = RequestMethod.GET)
    public ModelAndView employeeAddHandler(HttpSession session) {
        ModelAndView view = new ModelAndView();
        view.setViewName("/Employees/Add");

        if( session.getAttribute("errors") != null ) {
            view.addObject( "errors", session.getAttribute("errors") );
            session.setAttribute("errors", null);
        }
        
        return view;
    }

    @RequestMapping(value = "/Employees/Add", method = RequestMethod.POST)
    public String categoryAddHandlerPOST(@Valid @ModelAttribute Employees employee, BindingResult result, HttpSession session) {
        validator.validate(employee, result);
        if (result.hasErrors()) {
            session.setAttribute("errors", result.getAllErrors());
            return "redirect:/Employees/Add";
        } else {
            employeesRepository.saveAndFlush(employee);
        }

        return "redirect:/Employees/Index";
    }

    @RequestMapping(value = "/Employees/Remove/{employeeId}", method = RequestMethod.GET)
    public String employeeRemoveHandler(@PathVariable Integer employeeId) {
        Employees em = employeesRepository.findByEmployeeId(employeeId);
        employeesRepository.delete(em);

        return "redirect:/Employees/Index";
    }
}
