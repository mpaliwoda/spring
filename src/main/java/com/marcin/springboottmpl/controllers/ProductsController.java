package com.marcin.springboottmpl.controllers;

import com.marcin.springboottmpl.repositories.ProductsRepository;
import com.marcin.springboottmpl.northwind.Products;
import com.marcin.springboottmpl.repositories.CategoriesRepository;
import com.marcin.springboottmpl.northwind.Categories;
import com.marcin.springboottmpl.validators.ProductsValidator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;

@Controller
public class ProductsController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProductsController.class);

    @Autowired
    protected ProductsRepository productsRepository;

    @Autowired
    protected ProductsValidator validator;

    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        binder.setValidator(validator);
    }

    @Autowired
    protected CategoriesRepository categoriesRepository;

    @RequestMapping(value = "/Products/Index/Category", method = RequestMethod.POST)
    public ModelAndView productsHandlerHelper(HttpServletRequest request, HttpSession session) {
        Integer categoryId = Integer.parseInt(request.getParameter("categories"));
        session.setAttribute("categoryId", categoryId);

        return productsHandler(session);
    }

    @RequestMapping(value = "/Products/Index", method = RequestMethod.GET)
    public ModelAndView productsHandler(HttpSession session) {
        LOGGER.debug("productsHandler()");

        ModelAndView view = new ModelAndView();
        view.setViewName("/Products/Index");
        Iterable<Categories> categories = categoriesRepository.findAll();
        view.addObject("Categories", categories);

        Iterable<Products> products;
        if ( session.getAttribute("categoryId") == null || Integer.parseInt( session.getAttribute("categoryId").toString() ) == -1 ) {
            products = null;
        } else {
            Integer categoryId = Integer.parseInt(session.getAttribute("categoryId").toString());
            products = productsRepository.findByCategoriesCategoryId(categoryId);
            view.addObject("catId", categoryId);
        }

        view.addObject("Products", products);
        
        return view;
    }

    private String ProductSessionHelper(Integer productId) {
        return "/Products/Edit/" + productId.toString();
    }

    @RequestMapping(value = "/Products/Edit/{productId}", method = RequestMethod.GET)
    public ModelAndView productHandler(@PathVariable Integer productId, HttpSession session) {
        ModelAndView view = new ModelAndView();
        view.setViewName("/Products/Edit");

        Products product = (Products) session.getAttribute(ProductSessionHelper(productId));
        if (product == null) {
            product = productsRepository.findByProductId(productId);
        } else {
            productsRepository.save(product);
            session.setAttribute(ProductSessionHelper(productId), null);
        }

        if (session.getAttribute("errors") != null) {
            view.addObject("errors", session.getAttribute("errors"));
            session.setAttribute("errors", null);
        }

        view.addObject("pr", product);
        Iterable<Categories> categories = categoriesRepository.findAll();
        view.addObject("Categories", categories);

        return view;
    }

    @RequestMapping(value = "/Products/Edit/{productId}", method = RequestMethod.POST)
    public String employeeHandlerPOST(@PathVariable Integer productId, HttpSession session, @ModelAttribute Products product, BindingResult result) {
        validator.validate(product, result);
        if (result.hasFieldErrors()) {
            session.setAttribute("errors", result.getAllErrors());
        } else {
            session.setAttribute(ProductSessionHelper(productId), product);
        }
        return "redirect:" + ProductSessionHelper(productId);
    }

    @RequestMapping(value = "/Products/Add", method = RequestMethod.GET)
    public ModelAndView productAddHandler(HttpSession session) {
        ModelAndView view = new ModelAndView();
        view.setViewName("/Products/Add");

        Iterable<Categories> categories = categoriesRepository.findAll();
        view.addObject("Categories", categories);

        if( session.getAttribute("errors") != null ) {
            view.addObject( "errors", session.getAttribute("errors") );
            session.setAttribute("errors", null);
        }
        
        return view;
    }

    @RequestMapping(value = "/Products/Add", method = RequestMethod.POST)
    public String categoryAddHandlerPOST(@ModelAttribute Products product, BindingResult result, HttpSession session) {
        validator.validate(product, result);
        if (result.hasFieldErrors()) {
            session.setAttribute("errors", result.getAllErrors());
            return "redirect:/Products/Add";
        } else {
            productsRepository.saveAndFlush(product);
        }

        return "redirect:/Products/Index/";
    }

    @RequestMapping(value = "/Products/Remove/{productId}", method = RequestMethod.GET)
    public String productRemoveHandler(@PathVariable Integer productId) {
        Products pr = productsRepository.findByProductId(productId);
        productsRepository.delete(pr);

        return "redirect:/Products/Index";
    }
}
