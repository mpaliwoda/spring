package com.marcin.springboottmpl.controllers;

import com.marcin.springboottmpl.repositories.CustomersRepository;
import com.marcin.springboottmpl.northwind.Customers;
import com.marcin.springboottmpl.validators.CustomersValidator;
import java.util.Random;
//import java.util.List;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
public class CustomersController {
    private static final Logger LOGGER = LoggerFactory.getLogger(IndexController.class);

    @Autowired
    protected CustomersRepository customersRepository;
    
    @Autowired
    protected CustomersValidator validator;
    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        binder.setValidator(validator);
    }
    
   @RequestMapping(value="/Customers/Index", method = RequestMethod.GET)
   public ModelAndView customersHandler() {
        LOGGER.debug("customersHandler()");

        ModelAndView view = new ModelAndView();
                view.setViewName("/Customers/Index"); 

        Iterable<Customers> customers = customersRepository.findAll();
                view.addObject("Customers", customers);
        
                return view;
    }

    private String CustomerSessionHelper( String customerId ) {
        return "/Customers/Edit/" + customerId;
    }
        
    @RequestMapping(value="/Customers/Edit/{customerId}", method = RequestMethod.GET)
    public ModelAndView categoryHandler( @PathVariable String customerId, HttpSession session ) {
        ModelAndView view = new ModelAndView();
            view.setViewName("/Customers/Edit");
            
        Customers customer  = (Customers) session.getAttribute(CustomerSessionHelper( customerId ));
        if ( customer == null ) {
             customer =  customersRepository.findByCustomerId( customerId );
        }  else {
             session.setAttribute(CustomerSessionHelper( customerId ), null);
             customersRepository.save(customer);
         }
        if( session.getAttribute("errors") != null ) {
            view.addObject( "errors", session.getAttribute("errors") );
            session.setAttribute("errors", null);
        }
        
         view.addObject("cus", customer); 
        
        return view;
    }
    
    @RequestMapping(value="/Customers/Edit/{customerId}", method = RequestMethod.POST)
    public String customerHandlerPOST( @ModelAttribute Customers customer, BindingResult result, @PathVariable String customerId, HttpSession session ) {
        validator.validate(customer, result);
        if(result.hasErrors()) {
            session.setAttribute("errors", result.getAllErrors());
        } else { 
            session.setAttribute( CustomerSessionHelper( customerId ), customer );
        }
        return "redirect:" + CustomerSessionHelper( customerId );       
    }
    
    @RequestMapping(value="/Customers/Add", method = RequestMethod.GET)
    public ModelAndView categoryAddHandler(HttpSession session) {
        ModelAndView view = new ModelAndView();
            view.setViewName("/Customers/Add"); 
        
         if( session.getAttribute("errors") != null ) {
            view.addObject( "errors", session.getAttribute("errors") );
            session.setAttribute("errors", null);
         }   
            
        return view;
    }
    
    public String GenerateId() {
        String Id = "";
        int max = 90;
        int min = 65;
        Random randomGenerator = new Random();
        for( int i = 0; i < 5; i++ ){
            int randomInt = randomGenerator.nextInt( (max - min) + 1 ) + min;
            Id += (char) randomInt;
        }
        
        return Id;
    }
    
    @RequestMapping(value="/Customers/Add", method = RequestMethod.POST)
    public String categoryAddHandlerPOST( @ModelAttribute Customers customer, BindingResult result, HttpSession session ) {
        String Id;
        do {
           Id = GenerateId();
        } while( customersRepository.findByCustomerId( Id ) != null );
         
        validator.validate(customer, result);
        if (result.hasErrors()) {
            session.setAttribute("errors", result.getAllErrors());
            return "redirect:/Customers/Add";
        } else {
            customer.setCustomerId( Id );
            customersRepository.saveAndFlush( customer );
        }
        
        return "redirect:/Customers/Index";
    }
    
    @RequestMapping(value = "/Customers/Remove/{customerId}", method = RequestMethod.GET)
    public String customerRemoveHandler( @PathVariable String customerId ) {
        Customers cus = customersRepository.findByCustomerId( customerId );
        customersRepository.delete( cus );

        return "redirect:/Customers/Index";
    }
}
