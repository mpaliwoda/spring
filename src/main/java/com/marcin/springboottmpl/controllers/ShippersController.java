package com.marcin.springboottmpl.controllers;

import com.marcin.springboottmpl.repositories.ShippersRepository;
import com.marcin.springboottmpl.northwind.Shippers;
import com.marcin.springboottmpl.validators.ShippersValidator;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
public class ShippersController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ShippersController.class);

    @Autowired
    protected ShippersRepository shippersRepository;

    @Autowired
    protected ShippersValidator validator;
    @InitBinder("shipper")
    protected void initBinder(WebDataBinder binder) {
        binder.setValidator(validator);
    }
    
    @RequestMapping(value = "/Shippers/Index", method = RequestMethod.GET)
    public ModelAndView shippersHandler() {
        LOGGER.debug("shippersHandler()");

        ModelAndView view = new ModelAndView();
        view.setViewName("/Shippers/Index");

        Iterable<Shippers> shippers = shippersRepository.findAll();
        view.addObject("Shippers", shippers);

        return view;
    }

    private String ShipperSessionHelper(Integer shipperId) {
        return "/Shippers/Edit/" + shipperId.toString();
    }

    @RequestMapping(value = "/Shippers/Edit/{shipperId}", method = RequestMethod.GET)
    public ModelAndView categoryHandler(@PathVariable Integer shipperId, HttpSession session) {
        ModelAndView view = new ModelAndView();
        view.setViewName("/Shippers/Edit");

        Shippers shipper = (Shippers) session.getAttribute(ShipperSessionHelper(shipperId));
        if (shipper == null) {
            shipper = shippersRepository.findByShipperId(shipperId);
        } else {
            session.setAttribute(ShipperSessionHelper(shipperId), null);
            shippersRepository.save(shipper);
        }
        
        if( session.getAttribute("errors") != null ) {
            view.addObject( "errors", session.getAttribute("errors") );
            session.setAttribute("errors", null);
        }
        
        view.addObject("ship", shipper);
        return view;
    }

    @RequestMapping(value = "/Shippers/Edit/{shipperId}", method = RequestMethod.POST)
    public String shipperEditHandler(@PathVariable Integer shipperId, HttpSession session, @ModelAttribute Shippers shipper, BindingResult result) {
        validator.validate(shipper, result);
        if( result.hasFieldErrors() ) {
            session.setAttribute("errors", result.getAllErrors());
        } else {
            session.setAttribute(ShipperSessionHelper(shipperId), shipper);
        }
        return "redirect:" + ShipperSessionHelper(shipperId);
    }

    @RequestMapping(value = "/Shippers/Add", method = RequestMethod.GET)
    public ModelAndView shipperAddHandler(HttpSession session) {
        ModelAndView view = new ModelAndView();
        view.setViewName("/Shippers/Add");

        if( session.getAttribute("errors") != null ) {
            view.addObject( "errors", session.getAttribute("errors") );
            session.setAttribute("errors", null);
        }
        
        return view;
    }

    @RequestMapping(value = "/Shippers/Add", method = RequestMethod.POST)
    public String shipperAddHandlerPOST(@ModelAttribute Shippers shipper, BindingResult result, HttpSession session) {
        validator.validate(shipper, result);
        if(result.hasFieldErrors()) {
             session.setAttribute("errors", result.getAllErrors());
             return "redirect:/Shippers/Add";
        } else {
             shippersRepository.saveAndFlush(shipper);
        }

        return "redirect:/Shippers/Index";
    }

    @RequestMapping(value = "/Shippers/Remove/{shipperId}", method = RequestMethod.GET)
    public String shipperRemoveHandler(@PathVariable Integer shipperId) {
        Shippers ship = shippersRepository.findByShipperId(shipperId);
        shippersRepository.delete(ship);

        return "redirect:/Shippers/Index";
    }
}
