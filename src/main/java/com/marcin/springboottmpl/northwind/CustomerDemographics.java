package com.marcin.springboottmpl.northwind;
// Generated 2015-10-23 22:04:49 by Hibernate Tools 4.3.1


import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name="CustomerDemographics"
    ,schema="dbo"
    ,catalog="NORTHWND"
)
public class CustomerDemographics  implements java.io.Serializable {


     private String customerTypeId;
     private String customerDesc;
     private Set<Customers> customerses = new HashSet<Customers>(0);

    /**
     *
     */
    public CustomerDemographics() {
    }

    /**
     *
     * @param customerTypeId
     */
    public CustomerDemographics(String customerTypeId) {
        this.customerTypeId = customerTypeId;
    }

    /**
     *
     * @param customerTypeId
     * @param customerDesc
     * @param customerses
     */
    public CustomerDemographics(String customerTypeId, String customerDesc, Set<Customers> customerses) {
       this.customerTypeId = customerTypeId;
       this.customerDesc = customerDesc;
       this.customerses = customerses;
    }
       /**
     *
     * @return
     */
    @Id 


    
    @Column(name="CustomerTypeID", unique=true, nullable=false)
    public String getCustomerTypeId() {
        return this.customerTypeId;
    }
    
    /**
     *
     * @param customerTypeId
     */
    public void setCustomerTypeId(String customerTypeId) {
        this.customerTypeId = customerTypeId;
    }

    /**
     *
     * @return
     */
    @Column(name="CustomerDesc")
    public String getCustomerDesc() {
        return this.customerDesc;
    }
    
    /**
     *
     * @param customerDesc
     */
    public void setCustomerDesc(String customerDesc) {
        this.customerDesc = customerDesc;
    }

    /**
     *
     * @return
     */
    @ManyToMany(fetch=FetchType.LAZY, mappedBy="customerDemographicses")
    public Set<Customers> getCustomerses() {
        return this.customerses;
    }
    
    /**
     *
     * @param customerses
     */
    public void setCustomerses(Set<Customers> customerses) {
        this.customerses = customerses;
    }




}


