package com.marcin.springboottmpl.northwind;
// Generated 2015-10-23 22:04:49 by Hibernate Tools 4.3.1


import java.math.BigDecimal;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name="Order Details"
    ,schema="dbo"
    ,catalog="NORTHWND"
)
public class OrderDetails  implements java.io.Serializable {


     private OrderDetailsId id;
     private Orders orders;
     private Products products;
     private BigDecimal unitPrice;
     private short quantity;
     private float discount;

    
    public OrderDetails() {
    }

    
    public OrderDetails(OrderDetailsId id, Orders orders, Products products, BigDecimal unitPrice, short quantity, float discount) {
       this.id = id;
       this.orders = orders;
       this.products = products;
       this.unitPrice = unitPrice;
       this.quantity = quantity;
       this.discount = discount;
    }
   
    
    @EmbeddedId

    
    @AttributeOverrides( {
        @AttributeOverride(name="orderId", column=@Column(name="OrderID", nullable=false) ), 
        @AttributeOverride(name="productId", column=@Column(name="ProductID", nullable=false) ) } )
    public OrderDetailsId getId() {
        return this.id;
    }
    
    
    public void setId(OrderDetailsId id) {
        this.id = id;
    }

    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="OrderID", nullable=false, insertable=false, updatable=false)
    public Orders getOrders() {
        return this.orders;
    }
    
    
    public void setOrders(Orders orders) {
        this.orders = orders;
    }

    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="ProductID", nullable=false, insertable=false, updatable=false)
    public Products getProducts() {
        return this.products;
    }
    
    
    public void setProducts(Products products) {
        this.products = products;
    }

    
    @Column(name="UnitPrice", nullable=false, scale=4)
    public BigDecimal getUnitPrice() {
        return this.unitPrice;
    }
    
    
    public void setUnitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
    }

    
    @Column(name="Quantity", nullable=false)
    public short getQuantity() {
        return this.quantity;
    }
    
    
    public void setQuantity(short quantity) {
        this.quantity = quantity;
    }

    
    @Column(name="Discount", nullable=false, precision=24, scale=0)
    public float getDiscount() {
        return this.discount;
    }
    
    
    public void setDiscount(float discount) {
        this.discount = discount;
    }
}


