package com.marcin.springboottmpl.northwind;
// Generated 2015-10-23 22:04:49 by Hibernate Tools 4.3.1


import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name="Shippers"
    ,schema="dbo"
    ,catalog="NORTHWND"
)
public class Shippers  implements java.io.Serializable {


     private int shipperId;
     private String companyName;
     private String phone;
     private Set<Orders> orderses = new HashSet<Orders>(0);

    
    public Shippers() {
    }

    
    public Shippers(int shipperId, String companyName) {
        this.shipperId = shipperId;
        this.companyName = companyName;
    }

    
    public Shippers(int shipperId, String companyName, String phone, Set<Orders> orderses) {
       this.shipperId = shipperId;
       this.companyName = companyName;
       this.phone = phone;
       this.orderses = orderses;
    }
       
    @Id 


    
    @Column(name="ShipperID", unique=true, nullable=false)
    public int getShipperId() {
        return this.shipperId;
    }
    
    
    public void setShipperId(int shipperId) {
        this.shipperId = shipperId;
    }

    
    @Column(name="CompanyName", nullable=false)
    public String getCompanyName() {
        return this.companyName;
    }
    
    
    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    
    @Column(name="Phone")
    public String getPhone() {
        return this.phone;
    }
    
    
    public void setPhone(String phone) {
        this.phone = phone;
    }

    
    @OneToMany(fetch=FetchType.LAZY, mappedBy="shippers")
    public Set<Orders> getOrderses() {
        return this.orderses;
    }
    
    
    public void setOrderses(Set<Orders> orderses) {
        this.orderses = orderses;
    }




}


