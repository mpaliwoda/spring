package com.marcin.springboottmpl.northwind;
// Generated 2015-10-23 22:04:49 by Hibernate Tools 4.3.1


import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name="Region"
    ,schema="dbo"
    ,catalog="NORTHWND"
)
public class Region  implements java.io.Serializable {


     private int regionId;
     private String regionDescription;
     private Set<Territories> territorieses = new HashSet<Territories>(0);

    
    public Region() {
    }
    

    public Region(int regionId, String regionDescription) {
        this.regionId = regionId;
        this.regionDescription = regionDescription;
    }

    
    public Region(int regionId, String regionDescription, Set<Territories> territorieses) {
       this.regionId = regionId;
       this.regionDescription = regionDescription;
       this.territorieses = territorieses;
    }
   
    
    @Id 

    
    @Column(name="RegionID", unique=true, nullable=false)
    public int getRegionId() {
        return this.regionId;
    }
    
    
    public void setRegionId(int regionId) {
        this.regionId = regionId;
    }

    
    @Column(name="RegionDescription", nullable=false)
    public String getRegionDescription() {
        return this.regionDescription;
    }
    
    
    public void setRegionDescription(String regionDescription) {
        this.regionDescription = regionDescription;
    }

    
    @OneToMany(fetch=FetchType.LAZY, mappedBy="region")
    public Set<Territories> getTerritorieses() {
        return this.territorieses;
    }
    
    
    public void setTerritorieses(Set<Territories> territorieses) {
        this.territorieses = territorieses;
    }




}


