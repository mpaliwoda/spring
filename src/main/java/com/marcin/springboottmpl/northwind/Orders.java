package com.marcin.springboottmpl.northwind;
// Generated 2015-10-23 22:04:49 by Hibernate Tools 4.3.1


import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table(name="Orders"
    ,schema="dbo"
    ,catalog="NORTHWND"
)
public class Orders  implements java.io.Serializable {


     private int orderId;
     private Customers customers;
     private Employees employees;
     private Shippers shippers;
     private Date orderDate;
     private Date requiredDate;
     private Date shippedDate;
     private BigDecimal freight;
     private String shipName;
     private String shipAddress;
     private String shipCity;
     private String shipRegion;
     private String shipPostalCode;
     private String shipCountry;
     private Set<OrderDetails> orderDetailses = new HashSet<OrderDetails>(0);

    
    public Orders() {
    }

    
    public Orders(int orderId) {
        this.orderId = orderId;
    }

    
    public Orders(int orderId, Customers customers, Employees employees, Shippers shippers, Date orderDate, Date requiredDate, Date shippedDate, BigDecimal freight, String shipName, String shipAddress, String shipCity, String shipRegion, String shipPostalCode, String shipCountry, Set<OrderDetails> orderDetailses) {
       this.orderId = orderId;
       this.customers = customers;
       this.employees = employees;
       this.shippers = shippers;
       this.orderDate = orderDate;
       this.requiredDate = requiredDate;
       this.shippedDate = shippedDate;
       this.freight = freight;
       this.shipName = shipName;
       this.shipAddress = shipAddress;
       this.shipCity = shipCity;
       this.shipRegion = shipRegion;
       this.shipPostalCode = shipPostalCode;
       this.shipCountry = shipCountry;
       this.orderDetailses = orderDetailses;
    }
   
    
    @Id 

    
    @Column(name="OrderID", unique=true, nullable=false)
    public int getOrderId() {
        return this.orderId;
    }
    
    
    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="CustomerID")
    public Customers getCustomers() {
        return this.customers;
    }
    
    
    public void setCustomers(Customers customers) {
        this.customers = customers;
    }

    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="EmployeeID")
    public Employees getEmployees() {
        return this.employees;
    }
    
    
    public void setEmployees(Employees employees) {
        this.employees = employees;
    }

    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="ShipVia")
    public Shippers getShippers() {
        return this.shippers;
    }
    
    
    public void setShippers(Shippers shippers) {
        this.shippers = shippers;
    }

    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="OrderDate", length=23)
    public Date getOrderDate() {
        return this.orderDate;
    }
    
    
    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="RequiredDate", length=23)
    public Date getRequiredDate() {
        return this.requiredDate;
    }
    
    
    public void setRequiredDate(Date requiredDate) {
        this.requiredDate = requiredDate;
    }

    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="ShippedDate", length=23)
    public Date getShippedDate() {
        return this.shippedDate;
    }
    
    
    public void setShippedDate(Date shippedDate) {
        this.shippedDate = shippedDate;
    }

    
    @Column(name="Freight", scale=4)
    public BigDecimal getFreight() {
        return this.freight;
    }
    
    
    public void setFreight(BigDecimal freight) {
        this.freight = freight;
    }

    
    @Column(name="ShipName")
    public String getShipName() {
        return this.shipName;
    }
    
    
    public void setShipName(String shipName) {
        this.shipName = shipName;
    }

    
    @Column(name="ShipAddress")
    public String getShipAddress() {
        return this.shipAddress;
    }
    
    
    public void setShipAddress(String shipAddress) {
        this.shipAddress = shipAddress;
    }

    
    @Column(name="ShipCity")
    public String getShipCity() {
        return this.shipCity;
    }
    
    
    public void setShipCity(String shipCity) {
        this.shipCity = shipCity;
    }

    
    @Column(name="ShipRegion")
    public String getShipRegion() {
        return this.shipRegion;
    }
    
    
    public void setShipRegion(String shipRegion) {
        this.shipRegion = shipRegion;
    }

    
    @Column(name="ShipPostalCode")
    public String getShipPostalCode() {
        return this.shipPostalCode;
    }
    
    
    public void setShipPostalCode(String shipPostalCode) {
        this.shipPostalCode = shipPostalCode;
    }

    
    @Column(name="ShipCountry")
    public String getShipCountry() {
        return this.shipCountry;
    }
    
    
    public void setShipCountry(String shipCountry) {
        this.shipCountry = shipCountry;
    }

//@OneToMany(fetch=FetchType.LAZY, mappedBy="orders", cascade=CascadeType.ALL, orphanRemoval=true )

    
    public Set<OrderDetails> getOrderDetailses() {
        return this.orderDetailses;
    }
    
    
    public void setOrderDetailses(Set<OrderDetails> orderDetailses) {
        this.orderDetailses = orderDetailses;
    }
}


