package com.marcin.springboottmpl.repositories;


import com.marcin.springboottmpl.northwind.OrderDetails;
import org.springframework.stereotype.Repository;


import com.marcin.springboottmpl.northwind.OrderDetailsId;
import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface OrderDetailsRepository extends JpaRepository<OrderDetails, OrderDetailsId> {
    
}