package com.marcin.springboottmpl.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

//import Categories;
//import CustomerDemographics;
import com.marcin.springboottmpl.northwind.Customers;
import org.springframework.data.jpa.repository.JpaRepository;
//import Employees;
//import OrderDetails;
//import OrderDetails;
//import OrderDetailsId;
//import Orders;
//import Products;
//import Region;
//import Shippers;
//import Suppliers;
//import Territories;

@Repository
public interface CustomersRepository  extends JpaRepository<Customers, Integer> {
    @Override
    Page<Customers> findAll(Pageable pageable);
    Customers findByCustomerId(String ID);
    Customers findByCompanyName( String Name );
}
