package com.marcin.springboottmpl.repositories;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import com.marcin.springboottmpl.northwind.Categories;
import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface CategoriesRepository  extends JpaRepository<Categories, Long> {

    @Override
    Page<Categories> findAll(Pageable pageable);
    Categories findByCategoryId(int Id);
    Categories findByCategoryName( String name );
}
