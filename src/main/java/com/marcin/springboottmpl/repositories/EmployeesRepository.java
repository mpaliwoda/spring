package com.marcin.springboottmpl.repositories;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

//import Categories;
//import CustomerDemographics;
//import Customers;
import com.marcin.springboottmpl.northwind.Employees;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

@Repository
public interface EmployeesRepository  extends JpaRepository<Employees, Integer> {
    @Override
    Page<Employees> findAll(Pageable pageable);
    Employees findByEmployeeId(Integer EmployeeID);
}
