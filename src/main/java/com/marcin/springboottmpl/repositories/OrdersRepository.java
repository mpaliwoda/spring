package com.marcin.springboottmpl.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import org.springframework.stereotype.Repository;


import com.marcin.springboottmpl.northwind.Orders;
import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface OrdersRepository extends JpaRepository<Orders, Integer> {
    @Override
    Page<Orders> findAll(Pageable pageable);
    Orders findByOrderId(Integer Id);
}