package com.marcin.springboottmpl.repositories;

import com.marcin.springboottmpl.northwind.Shippers;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

//import Categories;
//import CustomerDemographics;
//import Customers;
//import Employees;
//import OrderDetails;
//import OrderDetails;
//import OrderDetailsId;
//import Orders;
//import Products;
//import Region;
import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface ShippersRepository  extends JpaRepository<Shippers, Integer> {
    @Override
    Page<Shippers> findAll(Pageable pageable);
    Shippers findByShipperId(Integer Id);
    Shippers findByCompanyName( String Name );
}
