package com.marcin.springboottmpl.repositories;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

import com.marcin.springboottmpl.northwind.Products;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

@Repository
public interface ProductsRepository extends JpaRepository<Products, Integer> {

    @Override
    Page<Products> findAll(Pageable pageable);        
    Products findByProductId(Integer ProductID);
    List<Products> findByCategoriesCategoryId( Integer CategoryID );
}
