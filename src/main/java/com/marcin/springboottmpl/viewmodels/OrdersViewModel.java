
package com.marcin.springboottmpl.viewmodels;


public class OrdersViewModel implements java.io.Serializable {
    private int orderId;
    private String customer;
    private String employee;
    private String shipper;
    private String orderDate;
    private String shippedDate;
    
    public int getOrderId() {
        return this.orderId;
    }
    
    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }
    
    public String getCustomer() {
        return this.customer;
    }
    
    public void setCustomer( String customer ) {
        this.customer = customer;
    }
    
    public String getEmployee() {
        return this.employee;
    }
    
    public void setEmployee( String employee ) {
        this.employee = employee;
    }
    
    public String getShipper() {
        return this.shipper;
    }
    
    public void setShipper( String shipper ) {
        this.shipper = shipper;
    }
    
    public String getOrderDate() {
        return this.orderDate;
    }
    
    public void setOrderDate( String orderDate ) {
        this.orderDate = orderDate;
    }
    
    public String getShippedDate() {
        return this.shippedDate;
    }
    
    public void setShippedDate( String shippedDate ) {
        this.shippedDate = shippedDate;
    }
    
}
