/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.marcin.springboottmpl.initializers;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 @author FireWater
 * https://spring.io/blog/2014/03/07/deploying-spring-boot-applications
 * https://raibledesigns.com/rd/entry/a_webapp_makeover_with_spring
 * http://stackoverflow.com/questions/30910642/webmvcconfigureradapter-does-not-work
 */
@Configuration
@ComponentScan
@SpringBootApplication
@EnableAutoConfiguration
@EnableJpaRepositories(basePackages = {"com.firewater.springboottmpl.*"})
@EntityScan(basePackages = {"com.firewater.springboottmpl.northwind"})
//@EnableIntegrationMBeanExport(registration = RegistrationPolicy.REPLACE_EXISTING)
//@PropertySource("classpath:application.properties")
@PropertySources(value = {@PropertySource("classpath:application.properties")})
//@ImportResource( { "classpath:applicationContext.xml", "classpath:archive-ticket-persistence.xml", "classpath:rest.properties", "classpath:complete-ticket-persistence.xml", "classpath:datasource.properties", "classpath:ticket-persistence.xml", "classpath:user-persistence.xml" } ) 
public class ApplicationStartingPoint extends SpringBootServletInitializer {
    
    public static void main(String[] args) {
        SpringApplication.run(ApplicationStartingPoint.class, args);
    }    
 
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(ApplicationStartingPoint.class);
        //return application.sources(applicationClass);
    }

    //private static final Class<ApplicationStartingPoint> applicationClass = ApplicationStartingPoint.class;
 }
