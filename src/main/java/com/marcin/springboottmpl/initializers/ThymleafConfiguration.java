/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.marcin.springboottmpl.initializers;

import com.github.mxab.thymeleaf.extras.dataattribute.dialect.DataAttributeDialect;
import nz.net.ultraq.thymeleaf.LayoutDialect;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/*
http://stackoverflow.com/questions/27214584/how-to-add-the-layout-dialect-to-spring-boot-thymeleaf-autoconfiguration-file
https://stackoverflow.com/questions/23531580/how-do-i-add-a-thymeleaf-dialect-to-spring-boot
https://github.com/mxab/thymeleaf-extras-data-attribute
*/

@Configuration
public class ThymleafConfiguration {
	@Bean
	public LayoutDialect layoutDialect() {
		return new LayoutDialect();
	}
    
	@Bean
	public DataAttributeDialect dataAttributeDialect() {
		return new DataAttributeDialect();
	}
    
    /*
    @Bean
    public SpringSecurityDialect securityDialect() {
        return new SpringSecurityDialect();
    } 
    */
}