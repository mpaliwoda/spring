function PostFormTo(obj, controller, action, arg) {
    if(!obj || !obj.name) {
        alert('Funkcja PostForm: brak obiektu lub obiekt nie ma nazwy');
        return;
    }
 
    var frm = $(obj).closest('form');
    if(!frm) { alert('Funkcja PostForm: brak formy'); return; }
    frm.append('<input name="PostBy" type="hidden" value="' + obj.name + '" />');
 
    //if(arg)
    //    frm.append('<input name="PostEx" type="hidden" value="' + arg + '" />');
 
    if(controller && controller.length > 0) {
        if(action && action.length > 0) {
            if(arg && arg.length > 0)
                frm[0].action = '/' + controller + '/' + action + '/' + arg;
            else
                frm[0].action = '/' + controller + '/' + action;
        }
        else
            frm[0].action = '/' + controller;
    }
 
    frm.submit();
}
 
//*************************************************************
 
function PostToPagerPage(obj, controller, action, page) {
    $('#Page_page').get(0).value = page;
    PostFormTo(obj, controller, action);
}
 
//*************************************************************
 
function PostFormToUrl(obj, url, arg) {
    if(!obj || !obj.name) {
        alert('Funkcja PostFormUrl: brak obiektu lub obiekt nie ma nazwy');
        return;
    }
 
    var frm = $(obj).closest('form');
    if(!frm) { alert('Funkcja PostForm: brak formy'); return; }
    frm.append('<input name="PostBy" type="hidden" value="' + obj.name + '" />');
 
    if(arg && arg.length > 0)
        frm[0].action = url + '/' + arg;
    else
        frm[0].action = url;
 
    frm.submit();
}
 
//*************************************************************
 
function PostToPagerPageUrl(obj, url, page, size) {
    $('#Page_page').get(0).value = page;
    $('#Page_size').get(0).value = size;
    PostFormToUrl(obj, url);
}
 
//*************************************************************
